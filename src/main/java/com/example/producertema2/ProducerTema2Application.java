package com.example.producertema2;

import com.example.producertema2.configure.MQConfig;
import com.example.producertema2.model.MeasurementMessage;
import com.example.producertema2.reader.SensorReader;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@SpringBootApplication
public class ProducerTema2Application {

	@Autowired
    private RabbitTemplate rabbitTemplate;

	public static void main(String[] args) {
		SpringApplication.run(ProducerTema2Application.class, args);
	}

	@Bean
	CommandLineRunner init() {
		return args ->
		{

			if(args.length > 0){
				LocalDateTime localDateTime = LocalDateTime.now();
				Date date = new Date(localDateTime.getYear(), localDateTime.getMonthValue(), localDateTime.getDayOfMonth(),
						localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
				System.out.println("\ndate in main = " + date);


				SensorReader sensorReader = new SensorReader();
				MeasurementMessage measurementAssignment2 =
						new MeasurementMessage(UUID.fromString(args[0]), LocalDateTime.now(), 3.0F);

				for(MeasurementMessage m: sensorReader.getMeasurements(
						UUID.fromString(args[0] )))
				{
					try {
						measurementAssignment2.setSensor_id(m.getSensor_id());
						measurementAssignment2.setDateTime(m.getDateTime());
						measurementAssignment2.setValue(m.getValue());
						System.out.println("valoare din sensor.csv = " + m.getValue());
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					try{
						rabbitTemplate.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY,
								measurementAssignment2);
					}
					catch (Exception e){
						e.printStackTrace();
					}
				}

			}



		};
	}

}
