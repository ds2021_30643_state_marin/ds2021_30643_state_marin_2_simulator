package com.example.producertema2.reader;


import com.example.producertema2.model.MeasurementMessage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;


public class SensorReader {

    public List<MeasurementMessage> getMeasurements(UUID idSensor) {
        //Long idSensor = 32L;

        List<MeasurementMessage> measurements = new ArrayList<>();
        try {
            FileReader fileReader = new
                    FileReader("C:\\Users\\gg7ur\\OneDrive\\Desktop\\sensor.csv");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String l;
            //Date date = new Date(2021-1900, 11-1, 14, 10, 25, 0);
            //System.out.println("\ndate = " + date.toString()+"\n");

            //LocalDateTime localDateTime = LocalDateTime.of(2021, 11, 14, 10, 25, 0);
            LocalDateTime localDateTime = LocalDateTime.now();
            System.out.println("\nlocalDateTime = " + localDateTime.toString()+"\n");

            //int i = 0;
            while (
//                    (
                            (l = bufferedReader.readLine()) != null
//                    )
//                            && (i < 16)
            ) {
                StringTokenizer stringTokenizer = new StringTokenizer(l);
                localDateTime = localDateTime.plusHours(1);
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Float measurementValue = Float.valueOf(stringTokenizer.nextToken());

                MeasurementMessage measurementAssignment2 = new
                        MeasurementMessage(idSensor, localDateTime, measurementValue);
                measurements.add( new MeasurementMessage(idSensor, localDateTime, measurementValue) );
                System.out.println("measurementAssign2 = " + measurementAssignment2);
                //i++;
                //idSensor++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return measurements;
    }




}
